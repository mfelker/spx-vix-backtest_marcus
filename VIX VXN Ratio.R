library("xts")
VIX.DF <- read.csv(file="http://www.cboe.com/publish/ScheduledTask/MktData/datahouse/vixcurrent.csv", header=FALSE, sep=",", skip=3 ,col.names=c("Date","Open","High", "Low", "Close"), colClasses=c("character","numeric","numeric","numeric","numeric"), stringsAsFactors=FALSE )
VXN.DF <- read.csv(file="http://www.cboe.com/publish/ScheduledTask/MktData/datahouse/vxncurrent.csv", header=FALSE, sep=",", skip=3 ,col.names=c("Date","Open","High", "Low", "Close"), stringsAsFactors=FALSE )
VIX.DF <- as.xts(VIX.DF, order.by=as.Date(VIX.DF[,1]), dateFormat='%m/%d/%Y')
VXN.DF <- as.xts(VXN.DF, order.by=as.Date(VXN.DF[,1]), dateFormat='%m/%d/%Y')
VIX.VXN.Ratio <- VIX.DF/VXN.DF